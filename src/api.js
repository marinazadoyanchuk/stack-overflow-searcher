const API_URL = 'https://api.stackexchange.com/2.2/search?site=stackoverflow&order=desc&sort=votes&filter=default'

export const getAnswers = ({title, page = 1}) => {
  return fetch(`${API_URL}&intitle=${title}&page=${page}`).then(res => res.json())
}