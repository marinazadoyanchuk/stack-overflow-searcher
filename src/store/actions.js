export const FETCH_DATA = 'FETCH_DATA'
export const SET_DATA = 'SET_DATA'

export const START_LOADING = 'SET_LOADING'
export const FINISH_lOADING = 'FINISH_LOADING'

export const SET_ERROR = 'SET_ERROR'
