import * as actions from './actions'


export function reducer(state, action) {
  // console.log({state, action})
  switch(action.type) {
    case actions.START_LOADING:
      return {
        ...state,
        loading: true,
        initial: false,
      }

    case actions.FINISH_lOADING:
      return {
        ...state,
        loading: false,
      }

    case actions.SET_DATA: {
      const {reqRes} = action.payload
      if (reqRes) {
        return {
          ...state,
          items: reqRes.items,
          hasMore: reqRes.hasMore,
          loading: false,
        }
      }

      return state
    }

    case actions.SET_ERROR: {
      return {
        ...state,
        error: action.payload.error,
      }
    }
    
    default:
      return state
  }
}