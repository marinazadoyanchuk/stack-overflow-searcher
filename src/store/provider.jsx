import React, {useReducer, useCallback, useMemo} from 'react'
import {START_LOADING, SET_DATA, SET_ERROR} from './actions'
import {reducer} from './reducer'

export const storeContext = React.createContext()


export const StoreProvider = ({children, initialState = {initial: true}}) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const wrappedDispatch = useCallback((action) => {
    if (action.async) {
      dispatch({type: START_LOADING})

      action.request(action.payload)
        .then((res) => {
          dispatch({...action, type: SET_DATA, payload: {...action.payload, reqRes: res}})
        })
        .catch((error) => {
          dispatch({type: SET_ERROR, payload: error})
        })
    } else {
      dispatch(action)
    }
  }, [])

  return <storeContext.Provider value={useMemo(() => [state, wrappedDispatch], [state, wrappedDispatch])}>
      {children}
    </storeContext.Provider>
}
