import React from 'react';
import './App.css';
import {StoreProvider} from './store/provider'
import {SearchNav} from './components/search-nav'
import {ResultArea} from './components/result-area/index'

function App() {
  return (
    <div className="App">
      <StoreProvider>
        <SearchNav />
        <ResultArea />
      </StoreProvider>
    </div>
  );
}

export default App;
