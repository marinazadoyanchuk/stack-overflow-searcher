import React, {useContext, useState, useEffect, useRef, useCallback} from 'react'
import {storeContext} from '../../store/provider'
import {LoaderComponent} from '../ui-components/loader'
import {QuestionItem} from './question-item'
import {SortPanel} from './sort-panel'
import './result-area.css'

export const ResultArea = () => {
  const [store] = useContext(storeContext)
  const [localItems, setItems] = useState([])
  const sortRules = useRef([])
  const {items = [], loading} = store

  const applySortRulesForItems = useCallback((items) => {
    const [key, order] = sortRules.current

    return [...items].sort((a, b) => {
      return order * (a[key] - b[key])
    })
  }, [])

  // sync local items with store
  useEffect(() => {
    setItems(applySortRulesForItems(store.items || []))
  }, [store.items, applySortRulesForItems])

  const sortItemsBy = useCallback(key => sortOrder => {
    sortRules.current = [key, sortOrder]

    setItems(applySortRulesForItems(localItems))
  }, [localItems, applySortRulesForItems])

  const filterItemsByTitle = useCallback((title) => {
    const filterTitle = (item) => title.length === 0 || item.title.toLowerCase().includes(title)
  
    setItems(applySortRulesForItems(items.filter(filterTitle)))
  }, [items, applySortRulesForItems])

  return <main className="result-area">
    <LoaderComponent hidden={!loading} />
    <section className={`${loading || store.initial ? 'hidden-section' : ''}`}>
      <SortPanel
        sortBy={sortItemsBy}
        filterByTitle={filterItemsByTitle}
      />
      {localItems.length === 0 
        ? <div>Noting not found ;( Try again</div>
        : localItems.map((item) => <QuestionItem key={item.question_id} {...item} />)
      }
    </section>
    {store.initial && <div>Let's start searching!</div>}
  </main>
}