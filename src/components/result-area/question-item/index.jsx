import React from 'react'
import {Avatar} from './avatar'
import {Info} from './info'
import './question-item.css'

export const QuestionItem = ({owner, tags, ...question}) => {
  return <article className="question-item">
    <Avatar owner={owner} />
    <Info question={question} />
  </article>
}