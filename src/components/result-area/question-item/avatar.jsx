import React from 'react'

export const Avatar = ({owner}) => {
  return <div className="avatar-card">
    <a href={owner.link} className="owner-link" target="_blank" rel="noopener noreferrer">
      <img src={owner.profile_image} alt={owner.display_name}/>
    </a>
    <div className="owner-details">
      <a href={owner.link} target="_blank" rel="noopener noreferrer">{owner.display_name}</a>
      <small>Reputation: {owner.reputation}</small>
    </div>
  </div>
}