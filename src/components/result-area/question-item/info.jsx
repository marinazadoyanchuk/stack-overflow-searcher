import React from 'react'
import {formatDate} from '../../../utils'
import logo from './so-logo.svg'


export const Info = ({question}) => {
  return <div className="question-info">
      <header className="info-header">
        <div className="score">
          <img alt="score" src={logo} />
          <span>{question.score}</span>
        </div>
        <h3>
          <a href={question.link} target="_blank" rel="noopener noreferrer" dangerouslySetInnerHTML={{ __html: question.title}}/>
        </h3>
      </header> 
      <small className="create-date">{formatDate(question.creation_date)}</small>
    </div>
}