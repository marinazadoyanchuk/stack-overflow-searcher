import React from 'react'
import {SortButton} from './sort-button'
import {InputComponent} from '../../ui-components/input'
import './sort-panel.css'



export const SortPanel = ({sortBy, filterByTitle}) => {
  return <div className="sort-panel">
    <div className="search-input">
      <span className="input-label">Search by title:</span>
      <InputComponent className="input-xs" onEnter={filterByTitle}/>
    </div>
    <SortButton 
      sortOrder={-1} 
      title="sort by creation date" 
      onSort={sortBy('creation_date')}
    />
    <SortButton 
      sortOrder={-1} 
      title="sort by score" 
      onSort={sortBy('score')}
    />
  </div>
}