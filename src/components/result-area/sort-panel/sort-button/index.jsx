import React, {useState} from 'react'

import AscIcon from './sort-asc.svg'
import DescIcon from './sort-desc.svg'
import './sort-button.css'

export const SortButton = ({sortOrder, onSort, title}) => {
  const [order, setOrder] = useState(sortOrder) // order is 1 or -1

  const toggleType = () => {
    const newOrder = -1 * order
    setOrder(newOrder)
    onSort && onSort(newOrder)
  }

  return <div className="sort-link" onClick={toggleType}>
    <img src={order === 1 ? AscIcon : DescIcon} alt="sort-by" className="icon"/>
    <span>{title}</span>
  </div>
}