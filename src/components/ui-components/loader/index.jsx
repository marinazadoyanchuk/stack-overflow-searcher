import React from 'react'
import logo from './logo.svg'
import './loader.css'

export const LoaderComponent = ({hidden}) =>
  <div className={`app-loader ${hidden ? 'hidden' : ''}`}>
    <img src={logo} className="logo" alt="logo" />
  </div>