import React, {useState, useCallback} from 'react'

export const InputComponent = ({placeholder, className, onEnter = () => ({}), onChange}) => {
  const [value, setValue] = useState('')

  const onInputChange = useCallback((event) => {
    setValue(event.target.value)
    onChange && onChange(event.target.value)
  }, [onChange])

  const onKeyPress = useCallback((event) => {
    if(event.key === 'Enter') {
      onEnter(value)
    }
  }, [value, onEnter])

  return <input placeholder={placeholder} 
                value={value} 
                onChange={onInputChange} 
                className={className}
                onKeyPress={onKeyPress}
          />
}