import React from 'react'

export const ButtonComponent = ({title, onClick, className}) => {
  return <button onClick={onClick} className={className}>{title}</button>
}