import React, {useContext, useCallback, useRef} from 'react'
import {storeContext} from '../../store/provider'
import {FETCH_DATA} from '../../store/actions'
import {getAnswers} from '../../api'
import {InputComponent} from '../ui-components/input'
import {ButtonComponent} from '../ui-components/button'
import './search-nav.css'

export const SearchNav = () => {
  const [, dispatch] = useContext(storeContext)
  const inputValue = useRef('')

  const onSearch = useCallback(() => {
    dispatch({type: FETCH_DATA, async: true, request: getAnswers, payload: {title: inputValue.current}})
  }, [dispatch])

  const onInputChange = useCallback((value) => {
    inputValue.current = value
  }, [])

  return <header className="search-nav">
    <h1 className="column">
      Stack overflow searcher
    </h1>
    <div className="column">
      <InputComponent 
        placeholder="start search..." 
        className="search-input" 
        onEnter={onSearch}
        onChange={onInputChange}
      />
      <ButtonComponent title="Search" onClick={onSearch} className="search-button" />
    </div>
  </header>
}